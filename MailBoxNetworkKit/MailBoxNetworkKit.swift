//
//  NetworkKit.swift
//  NetworkKit
//
//  Created by Hager on 3/14/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

public class NetworkKit {
    
    public init (){
        print("Class has been initialised")
    }
    
    public func doSomething(){
        print("Yeah, it works")
    }
}
