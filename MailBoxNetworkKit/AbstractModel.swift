//
//  AbstractModel.swift
//  MailBoxNetworkKit
//
//  Created by Hager on 3/15/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Gloss

public class AbstractModel: Decodable {
    public init() {}
    required public init?(json: JSON) {
    }
}
