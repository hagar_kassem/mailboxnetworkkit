//
//  APIClient.swift
//  MailBoxNetworkKit
//
//  Created by Hager on 3/12/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import Foundation
import Alamofire

//TODO:- Alamorifire requests

public class APIClient {
    
    func request(MailsWebService webService: URLRequest?,
                     successHandler: @escaping (DataResponse<Any>)->Void,
                     failureHandler: @escaping (Error)->Void) {
        
        Alamofire
        .request(webService!)
        .validate()
        .responseJSON {
            (response: DataResponse<Any>) -> Void in
                switch response.result {
                    case .success:
                        successHandler(response)
                    case .failure(let error):
                        failureHandler(error)
                }
        }
    }
}
