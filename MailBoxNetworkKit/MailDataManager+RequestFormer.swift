//
//  MailDataManager+RequestFormer.swift
//  MailBoxNetworkKit
//
//  Created by Hager on 3/14/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import SystemConfiguration

extension MailDataManager: RequestFormer {
    
    func requestWithString(requestURLString: String) -> URLRequest? {
        var urlRequest: URLRequest? = nil
        if let url = NSURL(string: requestURLString) {
            urlRequest = configuredUrlRequest(FromURL: url as URL)
        }
        return urlRequest
    }
    private func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    private func configuredUrlRequest(FromURL url: URL) -> URLRequest {
        
        var urlRequest = URLRequest(url: url as URL)
        urlRequest.cachePolicy = requestCachePolicy()
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
    private func requestCachePolicy() -> URLRequest.CachePolicy {
        
        return isInternetAvailable() ? .reloadIgnoringLocalCacheData : .returnCacheDataDontLoad
    }
    
    
}
