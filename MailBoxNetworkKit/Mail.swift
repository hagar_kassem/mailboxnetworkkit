//
//  Mail.swift
//  MailBoxNetworkKit
//
//  Created by Hager on 2/8/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Gloss

public class Mail: AbstractModel {
    
    public var typeImageName: String!
    public var senderName: String!
    public var sendingDate: String!
    public var mailPreviewImageLink: String!
    public var scannedPapersNumber: String!
    
    public override init() {
        super.init()
        
//        let abs: AbstractModel = Mail()
//        let mail: Mail = AbstractModel()
    }
    
    required public init?(json: JSON) {
//        super.init(json: JSON)
        super.init()
        self.typeImageName = "typeImageName" <~~ json
        self.senderName = "senderName" <~~ json
        self.sendingDate = "sendingDate" <~~ json
        self.mailPreviewImageLink = "mailPreviewImageLink" <~~ json
        self.scannedPapersNumber = "scannedPapersNumber" <~~ json
    }
    
    public init(typeImageName: String,
         senderName: String,
         sendingDate: String,
         mailPreviewImageLink: String,
         scannedPapersNumber: String
         ) {
        super.init()
        self.typeImageName = typeImageName
        self.senderName = senderName
        self.sendingDate = sendingDate
        self.mailPreviewImageLink = mailPreviewImageLink
        self.scannedPapersNumber = scannedPapersNumber
    }
    
    

}
