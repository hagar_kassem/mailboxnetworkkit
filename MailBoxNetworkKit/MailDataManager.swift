//
//  MailDataManager.swift
//  MailBoxNetworkKit
//
//  Created by Hager on 3/12/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

//TODO:- Singlton object, define APICLient, completion bock

protocol RequestFormer {
    func requestWithString(requestURLString:String) -> URLRequest?
}

public class MailDataManager {
    
    private static var instance = MailDataManager()
    private var apiClient = APIClient()
    
    //MARK:- Singlton object
    public static func sharedInstance() -> MailDataManager {
        
        return instance
    }
    
    public func requestNewMails(FromURLWithString url: String,
                         ForModelType modelType: AbstractModel.Type,
                         successHandler: @escaping ([Mail])->Void,
                         failureHandler: @escaping (Error)->Void) {
        if let validRequest = requestWithString(requestURLString: url) {
            apiClient.request(MailsWebService: validRequest,
                              successHandler: { (mails: DataResponse<Any>) in
                                if modelType is Mail.Type {
                                    guard let mails = [Mail].from(jsonArray: mails.result.value as! [JSON]) else {
                                        debugPrint("Deserilization Failed")
                                        return
                                    }
                                    debugPrint("Deserilization Succeed")
                                    successHandler(mails)
                                }
            },
                              failureHandler: { (error: Error) in
                                debugPrint("\(error)")
                                failureHandler(error)
            })
        }
        else {
            debugPrint("Invalid Request")
        }
    }
}
